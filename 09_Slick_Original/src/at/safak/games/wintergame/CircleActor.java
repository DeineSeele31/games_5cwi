package at.safak.games.wintergame;



import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor {
	private float x,y;
	private float radius;
	
	
	
	public CircleActor(float x, float y, float radius) {
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void update(GameContainer gc, double d) {
		//this.x++;
		if(this.radius == 10) {
			this.y+= 0.3;
		}
		if(this.radius == 20) {
			this.y+= 0.6;
		}
		if(this.radius == 30) {
			this.y+= 0.9;
		}
		if(this.radius == 25) {
			this.x+= 0.9;
		}
		
		
		if (this.y>1000){
			Random r = new Random();
			int val = r.nextInt(725);
			int val2 = r.nextInt(1200);
			this.x=val;
			this.y=-val2;
		}
		
	}
	
	public void render(Graphics graphics) {
		graphics.fillOval(this.x, this.y, this.radius, this.radius);
	}
}
