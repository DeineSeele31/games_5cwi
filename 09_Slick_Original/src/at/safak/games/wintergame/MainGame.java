package at.safak.games.wintergame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame{
	

	private List<CircleActor> ca;
	private ShootingStar ss;
	private SnowMan sm;
	
	public MainGame(String title) {
		super(title);
		this.ca = new ArrayList<CircleActor>();
		// TODO Auto-generated constructor stub
	}

	
	
	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// gezeichnet
		
		for (CircleActor ca : ca) {
			((CircleActor) ca).render(graphics);
		}
		
		this.ss.render(graphics);
		this.sm.render(graphics);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		
		genSnowballs();
		genShootingStar();
		genSnowMan();
		
	}

	private void genSnowMan() {
		this.sm = new SnowMan(375,0);
	}
	
	private void genShootingStar() {
		this.ss = new ShootingStar(0,0);
	}
	
	private void genSnowballs() {
		Random r = new Random();
		int val = r.nextInt(725);
		//klein
		for (int i = 0; i < 50; i++) {
			CircleActor ca1;
			ca1 = new CircleActor(-400,val,10);
			ca.add(ca1);
		}
		//mittel
		for (int i = 0; i < 35; i++) {
			CircleActor ca2;
			ca2 = new CircleActor(-400,val,20);
			ca.add(ca2);
		}
		//gross
		for (int i = 0; i < 25; i++) {
			CircleActor ca3;
			ca3 = new CircleActor(-400,val,30);
			ca.add(ca3);
		}
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = zeit seit dem letzten aufruf
		
		for (CircleActor ca : ca) {
			((CircleActor) ca).update(gc, delta);
		}
		
		this.ss.update(gc,delta);
		this.sm.update(gc,delta);
		
		
	}
	
	@Override
	public void keyPressed(int key, char c) {
		if (key == Input.KEY_SPACE) {
			float x1 = this.sm.getX();
			float y1 = this.sm.getY();
			
			CircleActor ca4;
			ca4 = new CircleActor(x1, y1, 25);
			ca.add(ca4);
		}
	}
	
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,800,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
