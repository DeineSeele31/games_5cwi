package at.safak.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar {

	private float x, y;
	private float radius;
	private int timeToNextView, counter;

	public ShootingStar(float x, float y) {
		super();
		this.x = x;
		this.y = y;
		this.radius = 50;
		this.timeToNextView = 6000;
		this.counter=0;
	}

	public void update(GameContainer gc, double delta) {
		if(this.timeToNextView > this.counter) {
			if(this.radius == 0) {
				this.x = 0;
				this.y = 0;
				this.radius = 0;
				this.counter += delta;
			} else {
				this.x += 0.65;
				this.y += 0.65;
				this.radius -= 0.125;
				this.counter += delta;
			}
		} else  {
			this.counter = 0;
			setShootingStarToNewPosition();
		}
	}

	private void setShootingStarToNewPosition() {
		Random r = new Random();
		int val = r.nextInt(750);
		this.x = val;
		this.y = 0;
		this.radius = 50;
	}

	public void render(Graphics graphics) {
		graphics.fillOval(this.x, this.y, this.radius, this.radius);
	}
}
