package at.safak.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class SnowMan {
	private float x, y, radius;
	
	public SnowMan (float y, float x) {
		super();
		this.y = y;
		this.x = x;
		this.radius = 50;
	
	}
	public void update(GameContainer gc, double d) {
		if (gc.getInput().isKeyDown(Input.KEY_UP)){
			if(!(y==50)) {
			this.y--;
			}
		}
		if (gc.getInput().isKeyDown(Input.KEY_DOWN)){
			if(!(y==750)) {
			this.y++;
			}
		}
	}
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	
	public void render(Graphics graphics) {
		graphics.fillOval((float) (this.x+5.75), this.y-50, this.radius-10, this.radius-10);
		graphics.fillOval((float) (this.x+3.1125), this.y-25, this.radius-5, this.radius-5);
		graphics.fillOval(this.x, this.y, this.radius, this.radius);
	}
}
