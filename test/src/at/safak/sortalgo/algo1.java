package at.safak.sortalgo;

public class algo1 {

	public int[] doSort(int[] toSort)
	{
		for(int i = 1; i < toSort.length; i++) {
			int temp = toSort[i];
			int j = i;
			while(i > 0 && toSort[j-1] > temp)
			{
				toSort[j] = toSort[j-1];
				j--;
			}
			toSort[j] = temp;
		}
		return toSort;
	}
}
