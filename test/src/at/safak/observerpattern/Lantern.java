package at.safak.observerpattern;

public class Lantern implements Observable{

	@Override
	public void inform() {
		System.out.println("Lantern!");
	}

}
