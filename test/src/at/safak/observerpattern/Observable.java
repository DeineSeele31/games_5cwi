package at.safak.observerpattern;

public interface Observable {
	public void inform();
}
