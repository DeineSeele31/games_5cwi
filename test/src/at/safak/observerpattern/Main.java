package at.safak.observerpattern;

public class Main {
	public static void main(String[] args) {
	
	Cristmas c = new Cristmas();
	Lantern l = new Lantern();
	TrafficLight tl = new TrafficLight();
	Sensor sensor = new Sensor();
	
	sensor.addObservable(l);
	sensor.addObservable(tl);
	sensor.addObservable(c);
	c.inform();
	l.inform();
	tl.inform();
	sensor.informAll();

	}
}

