package at.safak.observerpattern;

import java.util.ArrayList;
import java.util.List;


public class Sensor {
	private List<Observable> obs;
	
	
	public Sensor() {
		this.obs = new ArrayList<Observable>();
	}
	
	public void addObservable(Observable obsvbl) {
		this.obs.add(obsvbl);
	}
	
	public void informAll() {
		for (Observable observable : obs) {
			System.out.println(observable + "Information an ALLE!");
		}
	}
}
