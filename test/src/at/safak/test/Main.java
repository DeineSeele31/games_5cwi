package at.safak.test;

public class Main {
	public static void main(String[] args) {
	    Engine e1 = new Engine(150, "diesel");
	    Engine e2 = new Engine(100, "benzin");
		
		Car c1 = new Car("red", e1);
	    Car c2 = new Car("blue", e2);
	    Car c3 = c2;
	    
	    System.out.println(c1.getEngine().getHorsePower());
	    
	    System.out.println(c1.getColor());
	    System.out.println(c2.getColor());
	    System.out.println();
	}
}
