package at.safak.remote;



public class Main {
	public static void main(String[] args) {
	    Battery b1 = new Battery(100);
	    Battery b2 = new Battery(80);
		
		Remote r1 = new Remote(b1,b2,50);
	    
		r1.turnOn();
		r1.turnOff();
	    r1.getStatus();
	    System.out.println(r1.getStatus());
	  
	}
}
