package at.safak.remote;


public class Remote {
	  private int hasPower;
	  private boolean isOn;
	  private int battery1;
	  private int battery2;
	  
	  public Remote(Battery battery1, Battery battery2, int hasPower) {
		// TODO Auto-generated constructor stub
		  
		  this.hasPower = hasPower;
		
	  }
	  
	  
	  
	  
	 public void turnOn() {
	    if(hasPower > 0) {
	    	hasPower = -5;
	    	this.isOn = true;
	    }
	    else
	    {
	    	this.isOn = false;
	    }
	  }
	  public void turnOff() {
		this.isOn = false;
	  }
	  public int getStatus() {
		int status;
		status = (battery1 + battery2)/2; 
		return status;
	  }
	  
	  /*
	  public String getModel() {
	    return model;
	  }
	  public void setModel(String model) {
	    this.model = model;
	  }

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	*/
}
