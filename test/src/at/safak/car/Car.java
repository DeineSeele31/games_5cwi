package at.safak.car;

public class Car {
	private String color;
	private int maxHp;
	private Producer producer;
	private int petrolUsage;
	private Engine engine;
	private int price;
	
	
	public Car(String color, int maxHp, Producer producer, int petrolUsage, Engine engine, int price) {
		super();
		this.color = color;
		this.maxHp = maxHp;
		this.producer = producer;
		this.petrolUsage = petrolUsage;
		this.engine = engine;
		this.price = price;
	}
	
	public String getDiscount() {
		return getDiscountRate() + "%";
	}

	private double getDiscountRate() {
		double dPrice;
		if(producer.getCountry() == "Germany") {
			dPrice = 60;
			return dPrice;
		} else if (producer.getCountry() == "Austria" && producer.getName() == "Harald") {
			dPrice = 40;
			return dPrice;
		}
		dPrice = price;
		return dPrice;
	}
	
	public double getPetrolUsage() {
		double petrol;
		if(petrolUsage > 50000) {
			petrol = petrolUsage * 1.098;
			return petrol;
		}
		petrol = petrolUsage;
		return petrol;
	}
	
	/* GETTERS & SETTERS */
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxHp() {
		return maxHp;
	}

	public void setMaxHp(int maxHp) {
		this.maxHp = maxHp;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setPetrolUsage(int petrolUsage) {
		this.petrolUsage = petrolUsage;
	}

}



