package at.safak.car;



public class Main {
	public static void main(String[] args) {
		 	Producer p1 = new Producer("Harald","Austria");
		    Producer p2 = new Producer("Stefan","Germany");
		    
		    Engine e1 = new Engine(140,"Benzin");
		    Engine e2 = new Engine(130,"Diesel");
		    
			Car c1 = new Car("red",200,p1,58000,e1,14000);
			Car c2 = new Car("blue",220,p2,30000,e2,12000);
			
			System.out.println(c1.getDiscount());
			System.out.println(c1.getPetrolUsage());
			System.out.println(c2.getDiscount());
			System.out.println(c2.getPetrolUsage());
			

			
			Person pe1 = new Person("franz", "m�ller",1999,10,2);
			Person pe2 = new Person("susi", "meier",1974,1,28);
			System.out.println(pe1.getBirthdate());
			System.out.println(pe1.getAge());
			
			pe1.addCar(c1);
			pe1.addCar(c2);
			
			pe1.printCars();
			
			System.out.println(pe1.getValueOfCars());
	}
}
