package at.safak.car;

public class Engine {
	private int kw;
	private String petrol;
	
	
	public Engine(int kw, String petrol) {
		super();
		this.kw = kw;
		this.petrol = petrol;
	}


	public int getKw() {
		return kw;
	}


	public void setKw(int kw) {
		this.kw = kw;
	}


	public String getPetrol() {
		return petrol;
	}


	public void setPetrol(String petrol) {
		this.petrol = petrol;
	}
	
	
}