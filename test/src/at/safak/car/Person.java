package at.safak.car;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Person {
	private String firstName;
	private String lastName;
	private LocalDate birthdate;
	private List<Car> cars;

	
	public Person(String firstName, String lastName, int year, int month, int day) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = new ArrayList<Car>();
		this.birthdate = LocalDate.of(year, month, day);
	}
	
	public void addCar (Car c) {
		this.cars.add(c);
	}
	public void printCars() {
		for (Car car : cars) {
			System.out.println(car.getColor());
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getBirthdate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
		String formattedString = birthdate.format(formatter);
		return formattedString;
	}
	
	public int getAge(){
	
		// validate inputs ...
		return Period.between(birthdate, LocalDate.now()).getYears();
	}
	
	public int getValueOfCars() {
		int PriceAll = 0;
		for (Car car : cars) {
			PriceAll += car.getPrice();
		}
		return PriceAll;
	}
	
}
