package at.safak.cesarenc;

public class Cesarenc implements Encryptor{
	@Override
	public void encrypt(String str) {
		String result = "";
		System.out.println("Encrypting the word: " + str);
		for (int i = 0;i < str.length(); i++){
		    int charValue = str.charAt(i);
		    String letter = String.valueOf( (char) (charValue + 2));
		    result += letter; 
		}
		System.out.println(result);
	}

	@Override
	public void decrypt(String str) {
		String result = "";
		System.out.println("Decrypting the word: " + str);
		for (int i = 0;i < str.length(); i++){
		    int charValue = str.charAt(i);
		    String letter = String.valueOf( (char) (charValue - 2));
		    result += letter;
		    
		}
		System.out.println(result);
	}

}
