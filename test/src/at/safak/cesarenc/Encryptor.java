package at.safak.cesarenc;

public interface Encryptor {
	public void encrypt(String str);
	public void decrypt(String str);
}
