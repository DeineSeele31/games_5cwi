package at.safak.bankomat;

import java.util.Scanner;

public class Einzahlen {
	 int deposited;

	 public void transaction() {
	  System.out.println("Wie viel Geld wollen sie aufladen.");
	  Scanner deposit = new Scanner(System.in);
	  int c = deposit.nextInt();
	  System.out.println("Sie haben " + c + " aufgeladen.");
	  deposited += c;
	 }

	 public int getDeposited() {
	  return deposited;
	 }

	 public void setDeposited(int deposited) {
	  this.deposited = deposited;
	 }
}
