package at.safak.library;

public class Main {
	public static void main(String[] args) {
		Player player = new Player();
		Song s1 = new Song();
		Title t1 = new Title();
		Item i1 = new Item();
		
		player.addPlayable(t1);
		player.addPlayable(s1);
		player.addPlayable(i1);
		
		player.playAll();
	}
}
