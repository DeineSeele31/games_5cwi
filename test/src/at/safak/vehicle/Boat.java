package at.safak.vehicle;

public class Boat extends Vehicle{
	private String propeller;
	private double discount;
	
	public Boat(String name, String color, int price, double discount, String propeller) {
		super(name, color, price);
		propeller = this.propeller;
		discount = this.discount;
		// TODO Auto-generated constructor stub
	}

	public String getPropeller() {
		return propeller;
	}

	public void setPropeller(String propeller) {
		this.propeller = propeller;
	}
	
	@Override
	public double getPrice() {
		double fee;
	    fee = this.price * discount;
	    return fee;
	}
}
