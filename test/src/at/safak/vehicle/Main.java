package at.safak.vehicle;

import java.util.ArrayList;
import java.util.List;



public class Main {
	
	public static void main(String[] args) {
		
	 	Producer p1 = new Producer("Harald","Austria");
	    Producer p2 = new Producer("Stefan","Germany");

	    System.out.println(p1.getCars());
	    System.out.println(p1.getName());
	    System.out.println(p1.getCountry());
	    System.out.println(p2.getCars());
	    System.out.println(p2.getName());
	    System.out.println(p2.getCountry());
	    
	    List<Car> cars;
	    List<Boat> boats;
	    cars = new ArrayList<Car>();
	    boats = new ArrayList<Boat>();
	    
		Car c1 = new Car("BMW","Black",20000,4);
		Car c2 = new Car("Mercedes","Black",20000,4);
		Car c3 = new Car("Porsche","Grey",20000,4);
		
		cars.add(c1);
		cars.add(c2);
		cars.add(c3);
		
		Boat b1 = new Boat("Pferd","Black",20000,0.85,"Propella1");
		Boat b2 = new Boat("B�r","White",20000,0.9,"Propella2");
		Boat b3 = new Boat("Schweinehund","Lila",20000,0.7,"Propella3");
		
		boats.add(b1);
		boats.add(b2);
		boats.add(b3);
		
	
		
		
		
		System.out.println(p1.getValueOfVehicles());
}
}
