package at.safak.vehicle;


import java.util.List;

import at.safak.vehicle.Car;;;


public class Producer {
	private List<Car> cars;
	private String name;
	private String country;
	private int ValueOfVehicles;
	
	public Producer(String name, String country) {
		super();
		this.name = name;
		this.country = country;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void addCars(List<Car> cars) {
		this.cars = cars;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public int getValueOfVehicles() {
		return ValueOfVehicles;
	}
}
