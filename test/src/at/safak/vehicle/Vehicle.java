package at.safak.vehicle;

public class Vehicle {
	private String name;
	private String color;
	protected double price;
	
	public Vehicle (String name, String color, double price) {
		name = this.name;
		color = this.color;
		price = this.price;
	}
	
	protected void Gas(int x) {
		System.out.println("**Brumm Brumm**");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
}
