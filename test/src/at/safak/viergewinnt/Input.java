package at.safak.viergewinnt;

import javax.swing.JOptionPane;

public class Input {
private String input;
	
	public void getInputField() {
		String input = JOptionPane.showInputDialog(null,
			 "Bitte geben Sie Ihren Namen ein",
			 "Spielername",
			 JOptionPane.QUESTION_MESSAGE);
		this.input = input;
	}
	
	public String getInput() {
		return input;
	}
	
}
