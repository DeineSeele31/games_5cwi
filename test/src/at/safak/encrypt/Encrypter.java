package at.safak.encrypt;

public interface Encrypter {
	public void encrypt(String b);
	public String getFounder();
}
